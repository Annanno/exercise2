package com.example.secondexercise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    //     1
    fun differElements(array: IntArray): Int {
        val set = mutableSetOf<Int>()
        for (i in array.indices)
            set.add(array[i])

        return set.size

    }

    //    2
    fun intersection(array1: IntArray, array2: IntArray): Set<Int> {
        return array1.intersect(array2.toList())
    }

    //    3
    fun unionArrays(array1: IntArray, array2: IntArray): Set<Int> {
        return array1.union(array2.toList())

    }

    //    4
    fun subSet(array: IntArray): MutableSet<Int> {
        var sum = 0
        val set = mutableSetOf<Int>()
        for (i in array.indices)
            sum += array[i]
        val mean = sum / array.size
        for (i in array.indices)
            if (array[i] < mean)
                set += array[i]
        return set
    }

    //    5
    fun findMaxMin(array: IntArray): String {
        array.sort()
        if (array.size <= 2)
            return "Second min or max doesn't exist"
        return "Min is ${array.distinct()[1]}, max is ${array.distinct()[array.distinct().size - 2]}"
    }

}